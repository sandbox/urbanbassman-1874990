<?php
/**
 * @file
 * coderdojo_content_type_dojo.features.inc
 */

/**
 * Implements hook_node_info().
 */
function coderdojo_content_type_dojo_node_info() {
  $items = array(
    'dojo' => array(
      'name' => t('Dojo'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
